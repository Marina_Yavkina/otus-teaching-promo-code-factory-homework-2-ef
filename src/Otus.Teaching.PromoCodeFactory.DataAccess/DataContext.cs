using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
         public DbSet<Role> Roles { get; set; }
        public DbSet<Employee> Employees { get; set; }
       
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<CustomerPreference> CustomerPreference { get; set; }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
            // Database.EnsureDeleted();
            // Database.EnsureCreated();
        }

        public DataContext()
        {
            // Database.EnsureDeleted();
            // Database.EnsureCreated();
        }
        //  protected override void OnModelCreating(ModelBuilder modelBuilder)
        // {
        //     modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
        //     modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
        //     modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
        //     modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);

        // }
    }
}