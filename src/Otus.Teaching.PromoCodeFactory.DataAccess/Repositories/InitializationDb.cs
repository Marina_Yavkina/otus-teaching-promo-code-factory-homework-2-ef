using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
public class DbInitializer : IDbInitializer
    {
         protected readonly DataContext _context;
        public DbInitializer(DataContext context)
        {
             _context = context;
          
            
        }
      public async void  Initialize(){
            _context.Database.EnsureDeleted();
            _context.Database.EnsureCreated();
 
        
            await _context.SaveChangesAsync();
             foreach (var e in FakeDataFactory.Employees)
            {
               _context.Employees.AddRange(e);
            }
            await _context.SaveChangesAsync();
            var emp = await _context.Employees.ToListAsync();
             var r = await _context.Roles.ToListAsync();

             foreach (var e in FakeDataFactory.Preferences)
            {
                _context.Preferences.Add(e);
            }
            await _context.SaveChangesAsync();

            foreach (var e in FakeDataFactory.Customers)
            {
                _context.Customers.Add(e);
            }
            await _context.SaveChangesAsync();

            


      }
}
}