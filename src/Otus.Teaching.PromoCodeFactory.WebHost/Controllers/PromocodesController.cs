﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<CustomerPreference> _cprRepository;
        public PromocodesController(IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository, IRepository<CustomerPreference> cpRepository, IRepository<PromoCode> promocodeRepository, IRepository<Employee> employeeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _cprRepository = cpRepository;
            _promocodeRepository = promocodeRepository;
            _employeeRepository = employeeRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            List<PromoCodeShortResponse> returnedPromo = new List<PromoCodeShortResponse>();
            foreach (var item in promocodes)
            {
                var resp = new PromoCodeShortResponse
                {
                    Id = item.Id,
                    Code = item.Code,
                    ServiceInfo = item.ServiceInfo,
                    BeginDate = String.Format("{0:MM-dd-yyyy}", item.BeginDate),
                    EndDate = String.Format("{0:MM-dd-yyyy}", item.EndDate),
                    PartnerName = item.PartnerName
                };
                returnedPromo.Add(resp);
            }
            return Ok(returnedPromo);

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost("{EmployeeId}")]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(Guid EmployeeId, GivePromoCodeRequest request)
        {
            // var preferences  = await _preferenceRepository.GetAllAsync();
            // var pr = preferences.Where(c => c.Name == request.Preference).FirstOrDefault();
            var cust_prefs = await _cprRepository.GetAllAsync();
            cust_prefs = cust_prefs.Where(c => c.Preference.Name == request.Preference).ToList();
            var pr = cust_prefs.Select(c => c.Preference).FirstOrDefault();

            List<Customer> customers = new List<Customer>();
            foreach (var c in cust_prefs)
            {
                customers.Add(c.Customer);
            }


            var employee = await _employeeRepository.GetByIdAsync(EmployeeId);
            foreach (var t in customers)
            {
                CustomerPreference cust_pr = new CustomerPreference
                {
                    Id = new Guid(),
                    CustomerId = t.Id,
                    Customer = t,
                    Preference = pr,
                    PreferenceId = pr.Id
                };

                await _cprRepository.Add(cust_pr);

                PromoCode code = new PromoCode
                {
                    Id = new Guid(),
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    PartnerManager = employee,
                    Preference = pr,
                    Customer = t,
                    EndDate = new DateTime(),
                    BeginDate = new DateTime()
                };
                await _promocodeRepository.Add(code);
            }
            return Ok();
        }
    }
}