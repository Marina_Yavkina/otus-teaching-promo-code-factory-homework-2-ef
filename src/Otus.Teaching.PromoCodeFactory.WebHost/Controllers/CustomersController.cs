﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<CustomerPreference> _cprRepository;

        public CustomersController(IRepository<Customer> customerRepository,
                    IRepository<Preference> preferenceRepository, IRepository<CustomerPreference> cpRepository, IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _cprRepository = cpRepository;
            _promocodeRepository = promocodeRepository;
        }
        /// <summary>
        /// Получить список всех заказчиков
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            try
            {
                var customer = await _customerRepository.GetAllAsync();
                if (customer != null)
                {
                    var query =
                        from c in customer.ToList()
                        select new CustomerShortResponse
                        {
                            Id = c.Id,
                            FirstName = c.FirstName,
                            LastName = c.LastName,
                            Email = c.Email
                        };
                    return Ok(query);
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
            return NotFound();
        }

        /// <summary>
        /// Получить данные заказчика по id
        /// </summary>
        /// <returns></returns>

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            try
            {

                var customer = await _customerRepository.GetByIdAsync(id);
                var promocodes = await _promocodeRepository.GetAllAsync();
                List<PromoCodeShortResponse> l = new List<PromoCodeShortResponse>();
                if (customer != null)
                {
                    var query = new CustomerResponse
                    {

                        Id = customer.Id,
                        FirstName = customer.FirstName,
                        LastName = customer.LastName,
                        Email = customer.Email
                    };
                    List<PromoCodeShortResponse> pr = new List<PromoCodeShortResponse>();
                    foreach (var p in customer.PromoCode)
                    {
                        pr.Add(new PromoCodeShortResponse
                        {
                            Id = p.Id,
                            Code = p.Code,
                            ServiceInfo = p.ServiceInfo,
                            BeginDate = p.BeginDate.ToShortDateString(),
                            EndDate = p.EndDate.ToShortDateString(),
                            PartnerName = p.PartnerName
                        });
                    }
                    if (pr.Count != 0)
                    {
                        query.PromoCodes = pr;
                    }
                    var cp = await _cprRepository.GetAllAsync();
                    var customerPreferences = cp.Where(c => (c.CustomerId == customer.Id)).Select(c =>
                        new Preference { Id = c.PreferenceId, Name = c.Preference.Name }).ToList();
                    if (customerPreferences != null)
                    {
                        query.Preferences = customerPreferences;
                    }

                    return Ok(query);
                }

            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }


            return NotFound();
        }


        /// <summary>
        /// добавить нового заказчика
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (ModelState.IsValid)
            {
                var customer = new Customer
                {
                    Id = new Guid(),
                    LastName = request.LastName,
                    FirstName = request.FirstName,
                    Email = request.FirstName
                };
                await _customerRepository.Add(customer);


                foreach (var p in request.PreferenceIds)
                {
                    var pr = await _preferenceRepository.GetByIdAsync(p);
                    await _cprRepository.Add(new CustomerPreference { Id = new Guid(), CustomerId = customer.Id, Customer = customer, PreferenceId = p, Preference = pr });
                }
                return Ok();
            }
            return BadRequest();
        }


        /// <summary>
        /// редактировать и сохранить данные заказчика 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (ModelState.IsValid)
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                customer.Email = request.Email;
                customer.LastName = request.LastName;
                customer.FirstName = request.FirstName;
                await _customerRepository.Update(customer);
                var cp = await _cprRepository.GetAllAsync();

                var ids_to_delete = cp.Where(c => (c.CustomerId == customer.Id)).ToList();
                foreach (var i in ids_to_delete)
                {
                    await _cprRepository.Delete(i.Id);
                }
                foreach (var p in request.PreferenceIds)
                {
                    var pr_cust = cp.Where(p => (p.PreferenceId == p.Id)).FirstOrDefault();
                    if (pr_cust == null)
                    {
                        var pr = await _preferenceRepository.GetByIdAsync(p);
                        var cust = await _customerRepository.GetByIdAsync(id);
                        await _cprRepository.Add(new CustomerPreference { Id = new Guid(), CustomerId = id, Customer = cust, PreferenceId = p, Preference = pr });
                    }
                }
                return Ok();
            }
            return BadRequest();
        }
        /// <summary>
        /// Удалить заказчика из БД с конкретным Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            foreach (var i in customer.PromoCode)
            {
                await _promocodeRepository.Delete(i.Id);
            }
            await _customerRepository.Delete(id);
            return Ok();
        }
    }
}