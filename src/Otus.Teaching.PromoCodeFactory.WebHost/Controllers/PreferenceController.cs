using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {

        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<CustomerPreference> _cprRepository;

    public PreferencesController(IRepository<Customer> customerRepository, 
            IRepository<Preference> preferenceRepository, IRepository<CustomerPreference> cpRepository, IRepository<PromoCode> promocodeRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _cprRepository = cpRepository;
            _promocodeRepository = promocodeRepository;
        }
        /// <summary>
        /// Возвращает весь список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetPeferencesAsync()
        {
            try 
            {

            var preference = await _preferenceRepository.GetAllAsync();
           
            if (preference != null)
            {
               var query =
                from c in preference.ToList()
                select new PreferenceResponse{
                  Id = c.Id,
                  Name = c.Name
                };
                return Ok(query);
            }
            }
            catch(Exception ex)
            {
             Console.Write(ex.Message);
            }

            
            return NotFound();
        }

    
    }
}