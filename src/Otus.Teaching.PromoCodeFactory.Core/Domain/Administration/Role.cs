﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(1000)]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }
        public virtual  ICollection<Employee>  Employees { get; set; }
    }
}