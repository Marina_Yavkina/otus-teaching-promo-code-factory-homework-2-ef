﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(1000)]
        public string FirstName { get; set; }
         [MaxLength(1000)]
        public string LastName { get; set; }

        [MaxLength(2001)]
        public string FullName => $"{FirstName} {LastName}";
        [MaxLength(100)]
        public string Email { get; set; }
        [ForeignKey("Role")]
        public virtual Guid RoleId {get;set;}
        public virtual Role Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}