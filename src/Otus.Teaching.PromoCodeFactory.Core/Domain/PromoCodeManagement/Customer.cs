﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(1000)]
        public string FirstName { get; set; }
        [MaxLength(1000)]
        public string LastName { get; set; }
        [MaxLength(2001)]

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(1000)]
        public string Email { get; set; }
        public virtual ICollection<CustomerPreference> CustomerPreference { get;set;}
        public virtual ICollection<PromoCode> PromoCode {get;set;}

        //TODO: Списки Preferences и Promocodes 
    }
}