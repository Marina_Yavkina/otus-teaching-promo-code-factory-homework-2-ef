﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
        public virtual ICollection<CustomerPreference> CustomerPreference { get;set;}
    }
}